package io.gitee.znnspace;

import java.util.Set;

/**
 * IOC容器
 * @author znn
 */
public interface Container {

    /**
     * 根据class获取bean
     * @param tClass
     * @return
     */
    <T> T getBean(Class<T> clazz);

    /**
     * 根据名称获取bean
     * @param name
     * @return
     */
    <T> T getBeanByName(String name);

    /**
     * 注册一个Bean到容器中
     * @param bean
     * @return
     */
    Object registerBean(Object bean);

    /**
     * 注册一个Class到容器中
     * @param clazz
     * @return
     */
    Object registerBean(Class<?> clazz);

    /**
     * 注册一个带名称的Bean到容器中
     * @param name
     * @param bean
     * @return
     */
    Object registerBean(String name,Object bean);

    /**
     * 删除一个Bean
     * @param clazz
     */
    void remove(Class<?> clazz);

    /**
     * 根据名称删除一个Bean
     * @param name
     */
    void removeByName(String name);

    /**
     * 返回所有Bean对象名称
     * @return
     */
    Set<String> getBeanNames();

    /**
     * 初始化装配
     */
    void initWired();
}
