package io.gitee.znnspace;

import io.gitee.znnspace.annotation.Autowired;

public class Lol {
    @Autowired
    private FaceService faceService;

    public void work() {
        faceService.buy("剑圣", 5);
    }

    public FaceService getFaceService() {
        return faceService;
    }
}
